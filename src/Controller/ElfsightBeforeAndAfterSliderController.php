<?php

namespace Drupal\elfsight_before_and_after_slider\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightBeforeAndAfterSliderController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/before-and-after-slider/?utm_source=portals&utm_medium=drupal&utm_campaign=before-and-after-slider&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
